<?php

use Illuminate\Support\Facades\Route;
// use Illuminate\Http\Request;

// Route::get('/', 'HomeController');
Route::get('search', 'SearchController@post')->name('posts.search');
Route::prefix('posts')->middleware('auth')->group(function () {
    Route::get('', 'PostController@index')->name('posts.index')->withoutMiddleware('auth');
    Route::get('create', 'PostController@create')->name('posts.create');
    Route::post('store', 'PostController@store');
    Route::get('{post:slug}/edit', 'PostController@edit')->name('posts.edit');
    Route::patch('{post:slug}/edit', 'PostController@update');
    Route::delete('{post:slug}/delete', 'PostController@destroy');
    Route::get('{post:slug}', 'PostController@show')->name('posts.show')->withoutMiddleware('auth');
});

Route::get('categories/{category:slug}', 'CategoryController@show');
Route::get('tags/{tag:slug}', 'TagController@show');
Route::view('contact', 'contact');
Route::view('about', 'about');

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');


// Route::get('/show', 'PostController@show');
// Route::get('/', function () {
//     return view('welcome');
// });

// Route::view('url', 'page');
// Route::get('/', function () {
//     $name = "Nursyafriady";
//     return view('welcome', ['namaku'=> $name]);
// });

// Route::get('/', function () {
//     return view('home');
// });

// Route::view('home', 'home');

// Route::view('contact', 'contact');
// Route::get('contact', function() {
//     // request()->fullUrl();
//     // return request()->path() == 'contact' ? 'sama' : 'tidak sama';
//     return request()->is('contact') ? 'sama' : 'tidak sama';
// });






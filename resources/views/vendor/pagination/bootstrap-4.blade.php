@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <!-- <span class="page-link" aria-hidden="true">&lsaquo;</span> -->
                    <span class="page-link rounded border border-info" aria-hidden="true">Previous</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link rounded border border-info" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                    <!-- &lsaquo; -->
                    Previous
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled rounded-md mx-1" aria-disabled="true">
                        <span class="page-link rounded-circle border border-dark">{{ $element }}</span>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item rounded mx-1 active" aria-current="page">
                                <span class="page-link rounded-circle border border-dark">{{ $page }}</span>
                            </li>
                        @else
                            <li class="page-item rounded mx-1">
                                <a class="page-link rounded-circle border border-dark" href="{{ $url }}">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link rounded border border-info" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        <!-- &rsaquo; -->
                        Next
                    </a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link rounded border border-info" aria-hidden="true">
                        <!-- &rsaquo; -->
                        Next
                    </span>
                </li>
            @endif
        </ul>
    </nav>
@endif

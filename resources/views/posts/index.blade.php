@extends('layouts.app')

@section('title', 'All Post')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="">
                @isset($category)
                    <h2 class="">Category "{{ $category->name }}"</h2>
                @endisset

                @isset($tag)
                    <h2 class="">Tag "{{ $tag->name }}"</h2>
                @endisset

                @if(!isset($category) && !isset($tag) )
                    <h2 class="">All Posts</h2>
                @endif
                <hr>
            </div>
            <div>
                @if(Auth::check())
                    <a href="{{  route('posts.create') }}" class="btn btn-success">Create New Post</a>
                @else
                    <a href="{{  route('login') }}" class="btn btn-success">login to create new post</a>
                @endif
            </div>
        </div>
        
        <div class="row justify-content-center">    
            @if($posts->count())
                @foreach($posts as $post)
                    <div class="col-md-6 d-flex">            
                        <div class="card mb-4 d-flex" style="width:50em">
                            <a href="{{ route('posts.show', $post->slug) }}">
                                <img style="height:20em; object-fit:cover; object-position:center;" 
                                     src="{{ asset($post->takeImage()) }}" class="card-img-top" 
                                     alt="{{ $post->title }}">
                            </a>

                            <div class="card-body" style="min-height:10em">
                                <div class="card-title" style="min-height:1em">
                                <a href="{{ route('posts.show', $post->slug) }}" >
                                    <h3>{{ $post->title }}</h3>
                                </a>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                      <small> ditulis oleh : {{ $post->author->name ?? ''}} </small>
                                    </div>
                                    <div class="text-secondary">
                                        <small> 
                                            Published on :  {{ ($post->updated_at->diffForHumans()) }}
                                        </small>                                   
                                    </div>
                                </div> 
                                <div class="text-secondary mt-3 mb-4">
                                    Kategori -          
                                    <a href="/categories/{{ $post->category->slug }}" class="btn btn-sm btn-success">
                                        {{ $post->category->name }}
                                    </a>              
                                </div>
                            </div>
                                <div style="min-height:10em">
                                    <p>{{ Str::limit($post->body, 300, ' ......') }}</p>
                                </div>
                                <!-- <a href="/posts/{{ $post->slug }}">Read More</a>  -->
                                
                                <div class="mt-3">
                                    Tags - 
                                    @foreach($post->tags as $tag)  
                                        <a href="/tags/{{ $tag->slug }}" class="btn btn-sm btn-warning">
                                            {{ $tag->name }}
                                        </a>         
                                    @endforeach  
                                </div>                              
                            </div>
                            
                        </div>           
                    </div>
                @endforeach
            @else   
                <div class="alert alert-info">
                    Upss,...There are no posts.
                </div>
            @endif
        </div>
        <div class="row d-flex justify-content-center">
            {{ $posts->links() }}
        </div>
    </div> 
@endsection
@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
    <div class="container d-flex justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Edit Post {{ $post->title }}
                </div>
                <div class="card-body">
                    <form action="/posts/{{ $post->slug }}/edit" method="post" enctype="multipart/form-data">
                        @method('patch')
                        @csrf
                        @include('posts.partials.form-control')
                    </form>
                </div>
            </div>
        </div>        
    </div> 
@endsection
@extends('layouts.app')

@section('title', $post->title)

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">           
                @if($post->thumbnail)
                    <img style="height:25em; object-fit:cover; object-position:center;" 
                        src="{{ asset($post->takeImage()) }}" class="rounded w-100" 
                        alt="{{ $post->title }}">
                @endif
                <h1>{{ $post->title }}</h1>
                <small>{{ $post->created_at->format("d F, Y") }} </small>
                {{ $post->created_at->isoFormat('dddd, D MMMM Y, HH:mm') }}
                <br>
                <div class="media my-3 mx-4 mb-4">
                    <img width="60" class="rounded-circle mr-3"  src="{{ $post->author->gravatar() }}" alt="">
                    <div class="media-body">
                        ditulis oleh : {{ $post->author->name }}
                        <br>
                        {{ '@' . $post->author->username }}   
                    </div>                         
                </div> 
                            
                <div class="text-secondary mt-3 mb-1">
                    Kategori           
                    <a href="/categories/{{ $post->category->slug }}" class="btn btn-success">
                        {{ $post->category->name }}
                    </a>              
                </div>
                <hr class="mt-3 mb-4">       
                <p>{!! nl2br($post->body) !!}</p>
                <hr>
                Tags  
                @foreach($post->tags as $tag)  
                    <a href="/tags/{{ $tag->slug }}" class="btn btn-warning">
                        {{ $tag->name }}
                    </a>         
                @endforeach    
                <div>
                    <!-- @auth : hanya untuk authentikasi user -->
                    <!-- @ifauth->user->id == $post->user_id -->
                    @if(auth()->user()->is($post->author))
                    <!-- Button trigger modal -->
                    <div class="d-flex mt-4">
                        @can('update', $post)
                            <a href="/posts/{{ $post->slug }}/edit" class="btn mr-2 btn-primary">EDIT</a>
                        @endcan
                    
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                        DELETE
                    </button>
                    </div>
                

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Apakah Anda benar ingin menghapus ?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div>
                                        <div>{{ $post->title }}</div>
                                        <div class="text-secondary"> 
                                            <small>Published on : {{ $post->created_at->isoFormat('dddd, D MMMM Y, HH:mm') }} </small>    
                                        </div>
                                    </div>
                                    <form action="/posts/{{ $post->slug }}/delete" method="post">
                                        @csrf
                                        @method('delete')
                                        <div class="d-flex justify-content-center mt-4">
                                            <button type="submit" class="btn btn-danger mr-3">YA</button>
                                            <button type="button" class="btn btn-success" data-dismiss="modal">TIDAK</button>
                                        </div>                                    
                                    </form>
                                </div>
                                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- @endauth -->
                </div>
            </div>
            <div class="col-md-4">
                @foreach($posts as $post)
                        <div class="col d-flex">            
                            <div class="card mb-4 d-flex" style="width:50em">
                                <a href="{{ route('posts.show', $post->slug) }}"></a>

                                <div class="card-body" style="min-height:10em">
                                    <div class="card-title" style="min-height:1em">
                                    <a href="{{ route('posts.show', $post->slug) }}" >
                                        <h3>{{ $post->title }}</h3>
                                    </a>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                        <small> ditulis oleh : {{ $post->author->name ?? ''}} </small>
                                        </div>
                                    </div> 
                                    <div class="text-secondary mt-3 mb-4">
                                        Kategori -          
                                        <a href="/categories/{{ $post->category->slug }}" class="btn btn-sm btn-success">
                                            {{ $post->category->name }}
                                        </a>              
                                    </div>
                                </div>
                                    <div style="min-height:10em">
                                        <p>{{ Str::limit($post->body, 300, ' ......') }}</p>
                                    </div>
                                    <!-- <a href="/posts/{{ $post->slug }}">Read More</a>  -->
                                    
                                    <div class="mt-3">
                                        Tags - 
                                        @foreach($post->tags as $tag)  
                                            <a href="/tags/{{ $tag->slug }}" class="btn btn-sm btn-warning">
                                                {{ $tag->name }}
                                            </a>         
                                        @endforeach  
                                    </div>                              
                                </div>
                                
                            </div>           
                        </div>
                @endforeach
            </div>
        </div>
    </div> 
@endsection
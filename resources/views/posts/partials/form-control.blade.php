<div class="form-group">
    <label for="thumbnail">Thumbnail</label>
    <input type="file" name="thumbnail" id="thumbnail" class="form-control @error('thumbnail') is-invalid @enderror" value="{{ old('thumbnail') ?? $post->thumbnail  }}">
    @error('thumbnail')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $post->title  }}">
    @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="category">Category</label>
    <select name="category" id="category" class="form-control @error('title') is-invalid @enderror">
        <option disabled selected>Choose Category</option>
        @foreach($categories as $category)
            <option {{ $category->id == $post->category_id ? 'selected' : '' }} value="{{ $category->id }}">
                        {{ old('category') ?? $category->name }}
            </option>
        @endforeach
    </select>
    @error('category')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="tags">Tags</label>
    <select name="tags[]" id="tags" class="form-control select2 @error('tags') is-invalid @enderror" multiple>
        <!-- <option disabled selected>Choose Tags</option> -->
        @foreach($post->tags as $tag)
            <option selected value="{{ $tag->id }}">
                {{ $tag->name }}
            </option>
        @endforeach
        @foreach($tags as $tag)
            <option value="{{ $tag->id }}">
                {{ $tag->name}}
            </option>
        @endforeach
    </select>
    @error('tags')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="body">Body</label>
    <textarea name="body" id="body" class="form-control @error('body') is-invalid @enderror">{{ old('body') ?? $post->body  }}</textarea>
    @error('body')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'EDIT' }}</button>

@push('after-script')
<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Choose Some Tags"
        });
    });
</script>
@endpush
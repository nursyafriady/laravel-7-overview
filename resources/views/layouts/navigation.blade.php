<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
      <a class="navbar-brand" href="#">LARAVEL 7</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item{{ request()->is('/') ? ' active' : '' }}">
            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item{{ request()->is('about') ? ' active' : '' }}">
            <a class="nav-link" href="/about">About</a>
          </li>
          <li class="nav-item{{ request()->is('contact') ? ' active' : '' }}">
            <a class="nav-link" href="/contact">Contact</a>
          </li>
          <li class="nav-item{{ request()->is('posts') ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('posts.index') }}">Post</a>
          </li>
          <li class="nav-item{{ request()->is('login') ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('login') }}">login</a>
          </li>
          <li class="nav-item{{ request()->is('register') ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('register') }}">Register</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto"> 
          <form action="{{ route('posts.search') }}" class="form-inline my-2 my-lg-0" method="get">
            <input name="query" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
          @auth
            <li class="nav-item dropdown d-flex justify-content-end">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name ?? '' }}
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                          @csrf
                      </form>
                  </div>
            </li>
          @endauth
        </ul>
      </div>
  </div>
</nav>
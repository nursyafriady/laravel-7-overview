<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ {Post, Category, Tag};
// use App\Category;
// use App\Tag;
use Illuminate\Support\Str;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth')->except(['index', 'show']);
    }
    
    public function index()
    {
        // return Post::get(['title', 'body']);
        // $posts = Post::get();
        $posts = Post::latest()->paginate(6);
        return view('posts.index', compact('posts'));
    }
    
    // public function show($slug)
    public function show(Request $request, $slug) //dianggap menseleksi ID
    {
        // $post = \DB::table('posts')->where('slug', $slug)->first();
        // $post = DB::table('posts')->where('slug', $slug)->first();
        // dd($post);
        // first()-> untuk menampilkan array collection ke dalam single raw di laravel
        // $post = \App\Post::where('slug', $slug)->first();
        $post = Post::where('slug', $slug)->firstOrFail();
        $posts = Post::where('category_id', $post->category_id)->latest()->limit(6)->skip(1)->get();
        // $posts = Post::with('author', 'tags', 'category')->where('category_id', $post->category_id)->get();
        // dd($posts);
        //  $post = Post::firstOrFail();
        //  dd($post);

        // if(is_null($post)) {
        //     // abort(404);
        //     return view('posts.404');
        // }

        // if(!$post) {
        //     // abort(404);
        //     return view('posts.404');
        // }
        // $post = Post::where('slug', $slug)->first();
        return view ('posts.show', compact('post', 'posts'));
    }

    public function create()
    {
        $submit = "CREATE";
        return view('posts.create', ([
            'submit' => $submit, 
            'post' => new Post(),
            'categories' => Category::get(),
            'tags' => Tag::get(),
        ]));
    }

    // public function store(Request $request)
    // {
        // dd($request);
        // $post = new Post;
        // $post->title = $request->title;
        // $post->slug = Str::slug($request->title);
        // $post->body = $request->body;

        // $post->save();

        // Post::create([
        //     'title' => $request->title,
        //     'slug' => Str::slug($request->title),
        //     'body' => $request->body,
        // ]);

        // dd($request->all());

        // $this->validate($request, [
        //     'title' => 'required',
        //     'body' => 'required',
        // ]);

        // $request->validate([
        //     'title' => 'required',
        //     'body' => 'required',
        // ]);

        // $attr = $request->validate([
        //     'title' => 'required',
        //     'body' => 'required',
        // ]);

        // $post = $request->all();
        // $attr['slug'] = Str::slug($request->title);
        // Post::create($attr);

        // return redirect()->to('posts');


        // $posts = Post::all();
        // return view('posts.create', compact('posts'));
    // }

    public function store(PostRequest $request)
    {
        // dd(request()->file('thumbnail'));
        
        $request->validate([
            'thumbnail' => 'image|mimes:jpeg,jpg,png,svg|max:2048'
        ]);

        $attr = $request->all();
        $slug = Str::slug(request()->title);
        $attr['slug'] = $slug;

        if(request()->file('thumbnail')) {
            $thumbnail = request()->file('thumbnail');
            // $thumbnailUrl = $thumbnail->storeAs("images/posts", "{$slug}.{$thumbnail->extension()}");
            $thumbnailUrl = $thumbnail->store("images/posts");
        } else {
            $thumbnailUrl = null;
        }
      
        
        // Validasi field
        // $attr = $this->validateRequest();
        $attr['category_id'] = request('category');
        $attr['thumbnail'] =  $thumbnailUrl;
        // $attr['user_id'] = auth()->id(); -> tidak direkomendasikan

        // create/simpan post ke DB
        $post = auth()->user()->posts()->create($attr);

        $post->tags()->attach(request('tags'));

        session()->flash('success', 'Post success added');

        return redirect()->to('posts');
    }

    public function edit(Post $post)
    {
        $categories = Category::get();
        $tags = Tag::get();
        $posts = Post::get();
        return view('posts.edit', compact('post','categories','tags', 'posts'));
    }

    public function update(PostRequest $request, Post $post)
    {
        // Validasi field
        // $attr = $this->validateRequest();
        $request->validate([
            'thumbnail' => 'image|mimes:jpeg,jpg,png,svg|max:2048'
        ]);

        $this->authorize('update', $post);

        if(request()->file('thumbnail')) {
            \Storage::delete($post->thumbnail);
        } else {
            $thumbnail = $post->thumbnail;
        }

        $thumbnail = request()->file('thumbnail') ?? null ;
        $thumbnailUrl = $thumbnail->store("images/posts") ?? null ;
        
        $attr = $request->all();
        $attr['category_id'] = request('category');
        $attr['thumbnail'] =  $thumbnailUrl;

        $post->update($attr);
        $post->tags()->sync(request('tags'));

        session()->flash('updated', 'Post success updated');
        return redirect()->to('posts');
    }

    public function destroy(Post $post)
    {
        // return view('posts.edit', compact('post'))        
        if(auth()->user()->is($post->author)) {
            \Storage::delete($post->thumbnail);
            $post->tags()->detach();
            $post->delete();        
            session()->flash('success', 'Post success deleted');
            return redirect()->to('posts');
        } else {
            session()->flash('success', 'Not your Post');
            return redirect()->to('posts');
        }    
    }

    public function validateRequest() 
    {
        return request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    }
}

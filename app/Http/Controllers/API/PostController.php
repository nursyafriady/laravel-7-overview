<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Http\Resources\PostCollection;

class PostController extends Controller
{
    public function index ()
    {
        return new PostCollection(Post::get());
    }
}

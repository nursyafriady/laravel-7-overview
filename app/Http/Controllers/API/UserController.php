<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
    public function index() 
    {
        // $users = User::all();
        // return $users;
        return new UserCollection(User::get());
    }
}

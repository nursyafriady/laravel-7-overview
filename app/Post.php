<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = 'posts';
    protected $fillable = ['title', 'slug', 'body', 'category_id', 'thumbnail'];
    protected $with = ['author', 'tags', 'category'];
    // protected $guarded = [];
    protected $hidden = [];

    public function scopeLatestFirst()
    {
        return $this->latest()->first();
    }

    public function scopeLatestPost()
    {
        return $this->latest()->get();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function takeImage()
    {
        return "storage/". $this->thumbnail;
    }

    // Memperkenalkan key pada route name (/posts/{post}) dari ID menjadi SLUG
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }
}
